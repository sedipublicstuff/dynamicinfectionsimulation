#include "population.h"
#include <QDebug>

Population::Population(QObject *parent) : QAbstractItemModel(parent)
{

}

QModelIndex Population::index(int row, int column, const QModelIndex &parent) const
{
    Q_UNUSED(parent)
       if (row < 0 || column<0) return QModelIndex();

       if (row >= m_persons.count()) {
           return  QModelIndex();
       }
       return createIndex(row, 0);
}

QModelIndex Population::parent(const QModelIndex &index) const {
    if (index.row() < 0 || index.column() <0) return QModelIndex();

    if (index.row() >= m_persons.count()) {
        return  QModelIndex();
    }
    return createIndex(index.row(), 0);}

QVariant Population::data(const QModelIndex &index, int role) const
{
    Q_UNUSED(role)
    return QVariant::fromValue(m_persons.at(index.row()));
}

QVector<Person *> Population::persons() const
{
    return m_persons;
}

void Population::setPersons(const QVector<Person *> &persons)
{
    m_persons = persons;
}

void Population::clear()
{
    for (auto p : m_persons) {
        p->deleteLater();
    }
}
