/*
 * Copyright (C) 2020 Sebastian Diel
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>
 */

#ifndef RENDERPAINTEDITEM_H
#define RENDERPAINTEDITEM_H

#include <QQuickPaintedItem>
#include "population.h"
#include "temporaryshape.h"

class RenderPaintedItem : public QQuickPaintedItem
{
    Q_OBJECT

public:
    RenderPaintedItem();
    void paint(QPainter *painter) override ;
    void repaint();

    Population *population() const;
    void setPopulation(Population *population);

    qreal dotRadius() const;
    void setDotRadius(const qreal &dotRadius);

    void appendToTemporaryShapes(TemporaryShape* shape);

//    void appendToMouseTrace(qreal x, qreal y) {
//        m_mouseTrace.append(QPointF(x,y));
//    }

    QVector<TemporaryShape *> tempShapes() const;

private:
    void geometryChanged(const QRectF &newGeometry, const QRectF &oldGeometry) override;

    Population * m_population = nullptr;
    qreal m_dotRadius = 5;

//    QVector<QPointF> m_mouseTrace;
    QVector<TemporaryShape*> m_tempShapes;
};

#endif // RENDERPAINTEDITEM_H
