/*
 * Copyright (C) 2020 Sebastian Diel
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>
 */

#include "person.h"
#include <QDateTime>

Person::Person():QObject(nullptr)
{

}

QPointF Person::pos() const
{
    return m_pos;
}

void Person::setPos(const QPointF &pos)
{
    if (pos != m_pos) {
        m_pos = pos;
        emit this->posChanged();
    }
}

qreal Person::speed() const
{
    return m_speed;
}

void Person::setSpeed(const qreal &speed)
{
    if (fabs(speed - m_speed) > 0.0001) {
        m_speed = speed;
        emit this->speedChanged();
    }
}

qreal Person::direction() const
{
    return m_direction;
}

void Person::setDirection(const qreal &direction)
{
    if (fabs(m_direction - direction) > 0.0001) {
        m_direction = direction;
        emit this->directionChanged();
    }
}

Person::HealthType Person::healthState() const
{
    return m_healthState;
}

Person::HealthType Person::visibleHealthState() const
{
    if (m_timeSinceInfection < m_timeToVisibility && m_healthState != HealthType::VACCINATED) {
        return HealthType::SUSCEPTIBLE;
    }
    return m_healthState;
}


void Person::setHealthState(const Person::HealthType &healthState)
{
    if (m_healthState != healthState) {
        m_healthState = healthState;
        emit this->healthStateChanged();
    }
}


QPointF Person::advanceInSpace(qreal maxX, qreal maxY) {
    if (m_healthState == HealthType::DECEASED) return m_pos;
    if (m_inIsolation) return m_pos;

    bool borderX = false;
    bool borderY = false;

    auto speedX = m_speed * cos(m_direction);
    auto speedY = m_speed * sin(m_direction);

    auto posX = m_pos.x() + speedX;
    auto posY = m_pos.y() + speedY;

    if (posX > maxX || posX < 0) {
        speedX = -speedX;
        borderX = true;
    }
    if (posY > maxY || posY < 0) {
        speedY = -speedY;
        borderY = true;
    }
    if (borderX || borderY) {
        this->setDirection(atan2(speedY, speedX));
    }
    this->setPos(QPointF(m_pos.x()+speedX, m_pos.y()+speedY));

    return m_pos;
}

void Person::infect()
{
    if (m_healthState == HealthType::VACCINATED) {
        return;
    }
    this->setHealthState(HealthType::INFECTED_ASYMPT);
    m_timeSinceInfection = 0;
    m_infectionHistory.insert(0, m_healthState);
    if (fastrand()%6 != 1) {
        m_willShowSymptoms = true;
        if (fastrand()%5 == 1) {
            m_willBecomeHeavy = true;
            if (fastrand()%3 == 1) {
                m_willDie = true;
            }
        }
    }
}

void Person::vaccinate()
{
    if (m_healthState == HealthType::SUSCEPTIBLE){
        m_healthState = HealthType::VACCINATED;
    }
}

void Person::advanceInTime(QVector<Person *> neighbours)
{
    //Workaround for artificially added infections: add missing history
    if (m_healthState != HealthType::SUSCEPTIBLE && m_infectionHistory.isEmpty()) {
        m_infectionHistory.insert(0, m_healthState);
    }
    // If in isolation, we wait for the time to pass, but we don't let anybody with visible symptoms out
    // N.b.: If isolation time is too short, asymptomatic infections might still be active!
    if (m_inIsolation) {
        m_isolationCounter--;
        if (m_isolationCounter < 0 &&
                m_healthState != HealthType::INFECTED_HEAVY
                && m_healthState != HealthType::INFECTED_LIGHT) {
            setInIsolation(false);
            m_isolationCounter = m_isolationTime;
            this->clearNeigbourHistory();
        }
    }

    // TrackingApp: remember all neighbours
    if (
            m_usesTrackingApp
            && m_healthState != HealthType::DECEASED
            && m_healthState != HealthType::RECOVERED
            && !m_inIsolation
            )
    {
        m_neighbourHistory.enqueue(neighbours);
        while (m_neighbourHistory.count() > m_timeToSymptoms ) {
            m_neighbourHistory.dequeue();
        }
    }

    // Here we shape the illness
    // first: becoming ill
    if (m_healthState == HealthType::SUSCEPTIBLE && !m_inIsolation) {
        int infectionPossibilities = 0;
        for (auto n : neighbours) {
            if (n->inIsolation()) continue;
            if (n->healthState() == HealthType::INFECTED_ASYMPT) infectionPossibilities += 4;
            else if (n->healthState() == HealthType::INFECTED_LIGHT) infectionPossibilities += 2;
            else if (n->healthState() == HealthType::INFECTED_HEAVY) infectionPossibilities += 1;
        }
        if (infectionPossibilities>0) {
            qreal infectionProbability = m_infectionConstant * infectionPossibilities/4;
            if (infectionProbability > 1) infectionProbability = 1;
            if (round(fmod(fastrand(),(1/infectionProbability))) == 0) {
                this->infect();
            }
        }
        // Progression of the disease
    } else {
        m_timeSinceInfection++;
        if (m_healthState == HealthType::INFECTED_ASYMPT) {
            if (m_willShowSymptoms) {
                if (m_timeSinceInfection - m_infectionHistory.keys().last() > m_timeToSymptoms) {
                    if (m_willBecomeHeavy) {
                        this->setHealthState(HealthType::INFECTED_HEAVY);
                    } else {
                        this->setHealthState(HealthType::INFECTED_LIGHT);
                    }
                    m_infectionHistory.insert(m_timeSinceInfection, m_healthState);
                }
            } else {
                if (m_timeSinceInfection - m_infectionHistory.keys().last() > m_timeToRecoveryFromLight) {
                    this->setHealthState(HealthType::RECOVERED);
                    m_infectionHistory.insert(m_timeSinceInfection, m_healthState);
                }
            }
        } else if (m_healthState == HealthType::INFECTED_HEAVY) {
            if (m_willDie) {
                if (m_timeSinceInfection - m_infectionHistory.keys().last() > m_timeToDeath) {
                    this->setHealthState(HealthType::DECEASED);
                    this->setInIsolation(false);
                    m_infectionHistory.insert(m_timeSinceInfection, m_healthState);
                }
            } else {
                if (m_timeSinceInfection - m_infectionHistory.keys().last() > m_timeToRecoveryFromHeavy) {
                    this->setHealthState(HealthType::RECOVERED);
                    m_infectionHistory.insert(m_timeSinceInfection, m_healthState);
                }
            }

        } else if (m_healthState == HealthType::INFECTED_LIGHT) {
            if (m_timeSinceInfection - m_infectionHistory.keys().last() > m_timeToRecoveryFromLight) {
                this->setHealthState(HealthType::RECOVERED);
                m_infectionHistory.insert(m_timeSinceInfection, m_healthState);
            }
        }
    }
}

bool Person::willDie() const
{
    return m_willDie;
}

void Person::setWillDie(bool willDie)
{
    m_willDie = willDie;
}

bool Person::inIsolation() const
{
    return m_inIsolation;
}

void Person::setInIsolation(bool inIsolation)
{
    m_inIsolation = inIsolation;
}

QQueue<QVector<Person *> > Person::neighbourHistory() const
{
    return m_neighbourHistory;
}

void Person::setNeighbourHistory(const QQueue<QVector<Person *> > &neighbourHistory)
{
    m_neighbourHistory = neighbourHistory;
}

qreal Person::infectionConstant() const
{
    return m_infectionConstant;
}

void Person::setInfectionConstant(const qreal &infectionConstant)
{
    m_infectionConstant = infectionConstant;
}

bool Person::usesTrackingApp() const
{
    return m_usesTrackingApp;
}

void Person::setUsesTrackingApp(bool usesTrackingApp)
{
    if (usesTrackingApp != m_usesTrackingApp) {
        m_usesTrackingApp = usesTrackingApp;
    }
}

Person::MotionType Person::motionType() const
{
    return m_motionType;
}

void Person::setMotionType(const MotionType &motionType)
{
    if (motionType != m_motionType) {
        m_motionType = motionType;
    }
}

QVector<QPointF> Person::attractors() const
{
    return m_attractors;
}

void Person::setAttractors(const QVector<QPointF> &attractors)
{
    m_attractors = attractors;
}

bool Person::directedTowardsAttractor() const
{
    return m_directedTowardsAttractor;
}

void Person::setDirectedTowardsAttractor(bool directedTowardsAttractor)
{
    m_directedTowardsAttractor = directedTowardsAttractor;
}
unsigned int Person::s_seed=uint(QDateTime::currentDateTime().toMSecsSinceEpoch());
