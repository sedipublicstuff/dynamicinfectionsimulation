/*
 * Copyright (C) 2020 Sebastian Diel
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>
 */

#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <QObject>
#include <QQmlApplicationEngine>
#include <QQuickView>
#include <QGuiApplication>

#include "person.h"
#include "population.h"
#include "renderpainteditem.h"
#include "diagrampainteditem.h"


/// ideas:
/// - timeToTracedIsolation (isolate a day or two later)
/// - vaccinate (man.) / automatic if "willDie"
/// - testing (man.)
///

class Controller : public QObject
{
    Q_OBJECT

    Q_PROPERTY(Population* population READ population WRITE setPopulation NOTIFY populationChanged)
    Q_PROPERTY(Controller::InteractionType interactionType READ interactionType WRITE setInteractionType NOTIFY interactionTypeChanged)
    Q_PROPERTY(int infectionConstantSliderValue READ infectionConstantSliderValue WRITE setInfectionConstantSliderValue NOTIFY infectionConstantSliderValueChanged)
    Q_PROPERTY(int populationCount READ populationCount WRITE setPopulationCount NOTIFY populationCountChanged)
    Q_PROPERTY(AutomationType automationType READ automationType WRITE setAutomationType NOTIFY automationTypeChanged)
    Q_PROPERTY(bool initialInfection READ initialInfection WRITE setInitialInfection NOTIFY initialInfectionChanged)
    Q_PROPERTY(int msBetweenSteps READ msBetweenSteps WRITE setMsBetweenSteps NOTIFY msBetweenStepsChanged)
    Q_PROPERTY(int percentOfTracingApps READ percentOfTracingApps WRITE setPercentOfTracingApps NOTIFY percentOfTracingAppsChanged)
    Q_PROPERTY(int  tracingAppAccuracy READ tracingAppAccuracy WRITE setTracingAppAccuracy NOTIFY tracingAppAccuracyChanged)
    Q_PROPERTY(GlobalMotionType globalMotionType READ globalMotionType WRITE setGlobalMotionType NOTIFY globalMotionTypeChanged)

    Q_PROPERTY(int weightCouchPotatoes READ weightCouchPotatoes WRITE setWeightCouchPotatoes NOTIFY weightCouchPotatoesChanged)
    Q_PROPERTY(int weightLocallyAttracted READ weightLocallyAttracted WRITE setWeightLocallyAttracted NOTIFY weightLocallyAttractedChanged)
    Q_PROPERTY(int weightCommuters READ weightCommuters WRITE setWeightCommuters NOTIFY weightCommutersChanged)
    Q_PROPERTY(int weightTravellers READ weightTravellers WRITE setWeightTravellers NOTIFY weightTravellersChanged)
    Q_PROPERTY(int numberOfAttractors READ numberOfAttractors WRITE setNumberOfAttractors NOTIFY numberOfAttractorsChanged)
    Q_PROPERTY(bool overloadState READ overloadState WRITE setOverloadState NOTIFY overloadStateChanged)

    Q_PROPERTY(QString qtVersion READ qtVersion WRITE setQtVersion NOTIFY qtVersionChanged)
    Q_PROPERTY(qreal preVaccinated READ preVaccinated WRITE setPreVaccinated NOTIFY preVaccinatedChanged)

public:
    enum class InteractionType {
        ISOLATE_20 = 0,
        ISOLATE_50 = 1,
        TRACK_20 = 2,
        TRACK_50 = 3,
        INFECT_1 = 4,
        INFECT_5 = 5,
        VACCINATE_20 = 6,
        VACCINATE_50 = 7

    };
    Q_ENUM(InteractionType)
    enum class AutomationType {
        NO_AUTO = 0,
        ISOLATE_DELAYED_AUTO = 1,
        ISOLATE_IMMEDIATELY_AUTO = 2,
        TRACK_DELAYED_AUTO = 3,
        TRACK_IMMEDIATELY_AUTO = 4,
        ISOLATE_ATTRACTOR_DILATORY = 5,
        ISOLATE_ATTRACTOR_NORMAL = 6,
        ISOLATE_ATTRACTOR_OUTRIGHT = 7,
    };
    Q_ENUM(AutomationType)
    enum class GlobalMotionType {
        MOVE_RANDOMLY = 0,
        MOVE_ATTRACTOR_BASED = 1
    };
    Q_ENUM(GlobalMotionType)

    Controller(QQmlApplicationEngine* engine, QGuiApplication* app);
    void init();

    Population* population() const;
    void setPopulation(Population *population);

    template <typename T>
    T findQmlElement(QObject* startingPoint, QString elementObjectName) {
        T element = nullptr;
        Q_ASSERT(startingPoint);
        QObject *target= startingPoint->findChild<QObject *>(elementObjectName);
        if (target) {
            element = qobject_cast<T>(target);
            if (!element) {
                qDebug()<<"QML-Element "<<elementObjectName<<" was found, but couldn't be casted. Wrong Type?";
            }
            return element;
        } else {
            for (auto child : startingPoint->children()) {
                element = findQmlElement<T>(child, elementObjectName);
                if (element) {
                    return element;
                }
            }
        }
        return nullptr;
    }

    static unsigned int s_seed;
    inline int fastrand() {
      s_seed = (214013*s_seed+2531011);
      return (s_seed>>16)&0x7FFF;
    }


    InteractionType interactionType() const;
    void setInteractionType(const InteractionType &interactionType);

    int infectionConstantSliderValue() const;
    void setInfectionConstantSliderValue(int infectionConstantSliderValue);

    int populationCount() const;
    void setPopulationCount(int populationCount);

    AutomationType automationType() const;
    void setAutomationType(const AutomationType &automationType);

    bool initialInfection() const;
    void setInitialInfection(bool initialInfection);

    int msBetweenSteps() const;
    void setMsBetweenSteps(int msBetweenSteps);

    int percentOfTracingApps() const;
    void setPercentOfTracingApps(int percentOfTracingApps);

    int tracingAppAccuracy() const;
    void setTracingAppAccuracy(int tracingAppAccuracy);

    void interactionVaccinate(qreal x, qreal y, qreal radius);

    void isolateAttractor(qreal x, qreal y, qreal radius);



    GlobalMotionType globalMotionType() const;
    void setGlobalMotionType(const GlobalMotionType &globalMotionType);

    Person::MotionType randomPersonMotionType();
    QVector<QPointF> adjacentGlobalAttractors(int count);
    QVector<QPointF> anyGlobalAttractors(int count);
    QPointF nearestAttractor(qreal x, qreal y);
    int weightLocallyAttracted() const;
    void setWeightLocallyAttracted(int weightLocallyAttracted);

    int weightCommuters() const;
    void setWeightCommuters(int weightCommuters);

    int weightTravellers() const;
    void setWeightTravellers(int weightTravellers);

    int weightCouchPotatoes() const;
    void setWeightCouchPotatoes(int weightCouchPotatoes);

    int numberOfAttractors() const;
    void setNumberOfAttractors(int numberOfAttractors);

    bool overloadState() const;
    void setOverloadState(bool overloadState);

    QString qtVersion() const;
    void setQtVersion(const QString &qtVersion);

    qreal preVaccinated() const;
    void setPreVaccinated(const qreal &preVaccinated);

signals:
    void populationChanged();
    void interactionTypeChanged();
    void infectionConstantSliderValueChanged();
    void populationCountChanged();
    void automationTypeChanged();
    void initialInfectionChanged();
    void msBetweenStepsChanged();
    void percentOfTracingAppsChanged();
    void tracingAppAccuracyChanged();
    void globalMotionTypeChanged();

    void weightLocallyAttractedChanged();
    void weightCommutersChanged();
    void weightTravellersChanged();
    void weightCouchPotatoesChanged();
    void numberOfAttractorsChanged();

    void overloadStateChanged();
    void qtVersionChanged();
    void preVaccinatedChanged();


public slots:
    void advance();
    void clicked(qreal x, qreal y);
    void restart();
    void lockdown();
    void halt();
    void infectionsDownToZeroSlot();


protected:
    QPointF randomPosition();
    QPointF randomBiasedPosition(QPointF attractor, qreal radius);
    void interactionInfect(qreal x, qreal y, qreal radius, int maxInfect);
private:
    void populatePopulation();
    int rasterColFromX(qreal x, qreal width);
    int rasterRowFromY(qreal y, qreal height);
    void insertIntoRaster(Person* p, qreal width, qreal height);
    QVector<Person*> neighbours(Person* p, qreal radius, qreal width, qreal height);
    qreal distanceSquared(Person* p1, Person* p2);
    qreal distanceSquared(QPointF p1, QPointF p2);
//    qreal distanceSquared(qreal x1, qreal y1, qreal x2, qreal y2);
    void sendSetToDiagram();
    QVector<Person*> personsWithinDistanceFrom(qreal x, qreal y, qreal distance);

    void interactionIsolate(qreal x, qreal y, qreal radius);
    void tracePerson(Person *p);
    void interactionTrace(qreal x, qreal y, qreal radius);
    qreal sliderValueToInfectionConstant(int sliderValue);

    Population* m_population;
    bool m_lockdown = false;
    bool m_halted = false;
    bool m_initialInfection = true;

    qreal m_dotRadius = 5;

    RenderPaintedItem* m_renderPaintedItem = nullptr;
    DiagramPaintedItem* m_diagramPaintedItem = nullptr;
    QQmlApplicationEngine * m_engine = nullptr;
    QVector<QVector<QVector<Person*>>> m_raster;
    int m_rasterRows = 160;
    int m_rasterCols = 320;
    qreal m_rasterCellWidth = -1;
    qreal m_rasterCellHeight = -1;

    InteractionType m_interactionType = InteractionType::TRACK_20;
    AutomationType m_automationType = AutomationType::TRACK_IMMEDIATELY_AUTO;

    int m_infectionConstantSliderValue = 100;
    int m_populationCount = -1; //will be overridden by init()


    qreal m_externalInfectionProbability = 0.001;
    int m_msBetweenSteps = 80;

    int m_percentOfTracingApps = 50;
    int m_tracingAppAccuracy = 50;

    QVector<QPointF> m_globalAttractors;
    GlobalMotionType m_globalMotionType = GlobalMotionType::MOVE_ATTRACTOR_BASED; // will be overridden by QML

    int m_weightLocallyAttracted = 50;
    int m_weightCommuters = 40;
    int m_weightTravellers = 5;
    int m_weightCouchPotatoes = 5;

    int m_numberOfAttractors = 60;

    bool m_overloadState = false;
    qreal m_preVaccinated = 0;

    QString m_qtVersion = QLatin1String(QT_VERSION_STR);
     QGuiApplication* m_app;

};
Q_DECLARE_METATYPE(Controller*);
Q_DECLARE_METATYPE(Controller::InteractionType);
Q_DECLARE_METATYPE(Controller::AutomationType);

#endif // CONTROLLER_H
