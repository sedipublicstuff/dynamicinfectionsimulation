/*
 * Copyright (C) 2020 Sebastian Diel
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>
 */

#ifndef POPULATION_H
#define POPULATION_H

#include <QObject>
#include <QAbstractItemModel>

#include "person.h"

class Population : public QAbstractItemModel
{
    Q_OBJECT
    Q_PROPERTY(QVector<Person*> persons READ persons WRITE setPersons NOTIFY personsChanged)

public:
    explicit Population(QObject *parent = nullptr);

    QModelIndex index(int row, int column = 0, const QModelIndex& parent = QModelIndex()) const;
    QModelIndex parent(const QModelIndex& index) const;

    Q_INVOKABLE int rowCount(const QModelIndex& parent = QModelIndex()) const { Q_UNUSED (parent) return m_persons.count(); }
    Q_INVOKABLE Person* personAt(int index) { return m_persons.at(index); }


    int columnCount(const QModelIndex& parent = QModelIndex()) const { Q_UNUSED (parent) return  m_persons.count(); }
    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const;

    QVector<Person *> persons() const;
    void setPersons(const QVector<Person *> &persons);

signals:

    void personsChanged();

public slots:
    void clear();

private:
    QVector<Person*> m_persons;

};

Q_DECLARE_METATYPE(Population*);

#endif // POPULATION_H
