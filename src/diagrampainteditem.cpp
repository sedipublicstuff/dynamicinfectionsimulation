#include "diagrampainteditem.h"

/*
 * Copyright (C) 2020 Sebastian Diel
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>
 */

#include <QPainter>

#include <QElapsedTimer>

DiagramPaintedItem::DiagramPaintedItem()
{
    m_colorMap.insert(0,QColor(Qt::blue));
    m_colorMap.insert(1,QColor(Qt::darkGreen));
    m_colorMap.insert(2,QColor(Qt::green));
    m_colorMap.insert(3,QColor(Qt::black));
    m_colorMap.insert(4,QColor(Qt::red));
}

void DiagramPaintedItem::paint(QPainter *painter)
{
    if (m_values.isEmpty()) return;

    qreal statWidth = 65;
     qreal paintWidth = width()-statWidth;

    qreal entryWidth = paintWidth/m_values.count();
    for (int setNr = 0; setNr < m_values.count(); setNr++) {
        qreal y = 0;
        for (int valNr = 0; valNr < m_values.at(setNr).count(); valNr++) {
            QColor c = m_colorMap.value(valNr,"white");
            painter->setPen(c);
            painter->setBrush(QBrush(c));
            qreal recHeight = m_values.at(setNr).at(valNr)*height();
            painter->drawRect(int(statWidth+setNr*entryWidth), int(y), int(entryWidth), int(recHeight));
            y += recHeight;
        }
    }
    auto font = painter->font();
    font.setWeight(QFont::Normal);
    painter->setFont(font);
    auto current = m_values.last();
    for (int valNr = 0; valNr < m_values.last().count(); valNr++) {
        QColor c = m_colorMap.value(valNr,"white");
        painter->setPen(c);
        painter->drawText(5,valNr*15, QString::number(qRound(1000*m_values.last().at(valNr))/10.0).append(" %"));
        if (!m_infectionsPresent && valNr == m_values.last().count()-1) {
            int size = font.pointSize();
            font.setWeight(QFont::ExtraBold);
            font.setPointSize(int(size * 3));
            painter->setFont(font);
            painter->setPen(QColor(Qt::green));
            painter->drawText(25, valNr * 15, QString(QChar(0x2713)));
        }
    }
}

void DiagramPaintedItem::appendValueSet(QVector<int> set)
{
    bool infectionsExistBefore = false;
    if (!m_values.isEmpty()) {
        infectionsExistBefore = m_values.last().last() > 0;
    }
    QVector<qreal> result;
    qreal sum = 0;
    for (auto r : set) {
        sum += r;
    }
    for (auto r : set) {
        result.append(r/sum);
    }
    m_values.append(result);
    m_infectionsPresent = set.last() != 0;
    if (infectionsExistBefore  && !m_infectionsPresent) {
        emit this->infectionsDownToZero();
    }


}

void DiagramPaintedItem::clearValues()
{
    m_values.clear();
}

void DiagramPaintedItem::repaint()
{
    this->update(QRect(0,0,qRound(width()), qRound(height())));
}


void DiagramPaintedItem::geometryChanged(const QRectF &newGeometry, const QRectF &oldGeometry)
{
    QQuickPaintedItem::geometryChanged(newGeometry, oldGeometry);
}
