/*
 * Copyright (C) 2020 Sebastian Diel
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>
 */

#ifndef TEMPORARYSHAPE_H
#define TEMPORARYSHAPE_H
#include <QtGlobal>
#include <QColor>
#include <QDebug>


class TemporaryShape
{
public:
    enum class ShapeType {
        LINE = 0,
        ELLIPSE = 1,
        RECT = 2,
    };
    TemporaryShape();
    TemporaryShape(ShapeType shape, qreal x1, qreal y1, qreal x2, qreal y2);

    qreal x1() const;
    void setX1(const qreal &x1);

    qreal y1() const;
    void setY1(const qreal &y1);

    qreal x2() const;
    void setX2(const qreal &x2);

    qreal y2() const;
    void setY2(const qreal &y2);

    int timeToLive() const;
    void setTimeToLive(int timeToLive);

    QColor color() const;
    void setColor(const QColor &color);

    qreal lineWidth() const;
    void setLineWidth(const qreal &lineWidth);


    ShapeType shape() const;
    void setShape(const ShapeType &shape);

    bool advance();
    int test() const;
    void setTest(int test);
    void incTest();

    bool isCongruent(TemporaryShape* sh2);

private:
    qreal m_x1 = 0;
    qreal m_y1 = 0;
    qreal m_x2 = 0;
    qreal m_y2 = 0;
    ShapeType m_shape = ShapeType::LINE;
    int m_timeToLive = 3;
    QColor m_color = QColor(Qt::yellow);
    qreal m_lineWidth = 1;

    int m_test = 0;
};

#endif // TEMPORARYSHAPE_H
