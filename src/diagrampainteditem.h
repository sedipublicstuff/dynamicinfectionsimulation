/*
 * Copyright (C) 2020 Sebastian Diel
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>
 */

#ifndef DIAGRAMPAINTEDITEM_H
#define DIAGRAMPAINTEDITEM_H

#include <QQuickPaintedItem>

class DiagramPaintedItem : public QQuickPaintedItem
{
    Q_OBJECT


public:
    DiagramPaintedItem();
    void paint(QPainter *painter) override ;

    void appendValueSet(QVector<int> set);
    void clearValues();

signals:
    void infectionsDownToZero();

public slots:
    void repaint();

private:
    void geometryChanged(const QRectF &newGeometry, const QRectF &oldGeometry) override;
    QMap<int, QColor> m_colorMap;
    QVector<QVector<qreal>> m_values;
    bool m_infectionsPresent = true;
};

#endif // DIAGRAMPAINTEDITEM_H
