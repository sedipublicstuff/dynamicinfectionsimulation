/*
 * Copyright (C) 2020 Sebastian Diel
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>
 */


import QtQuick 2.14
import QtQuick.Controls 2.14
import QtQuick.Window 2.14
import de.containment.herrdiel 1.0

Window {
    visible: true
    width: Qt.application.screens[0].width
    height: Qt.application.screens[0].height
    x: 0 // Qt.application.screens[0].virtualX
    y: 0 // screen.virtualY
    title: qsTr("Simulation einer Epidemie")
    id: mainWindow
    Component.onCompleted: { showMaximized();  } // Workaround until above screen placement works

    property int rowHeight: 40
    property int titleHeight: 20
    property int spacerWidth: 3
    screen: Qt.application.screens[0]

    //  @disable-check M16
    onClosing: {
        if (settingsPopup.opened) {
            settingsPopup.close()
            close.accepted = false
        } else {
            close.accepted = true
        }
    }

    Column {
        width: parent.width
        height: parent.height
        Row {
            id: topRow
            width: parent.width
            height: rowHeight

            Button {
                text: " Info "
                height: parent.height
                onClicked: {
                    aboutDialog.open()
                }
            }
            Rectangle {
                height: rowHeight
                width: spacerWidth
                color: "transparent"
            }

            Button {
                text: "Neustart"
                onClicked: controller.restart()
            }
            Rectangle {
                height: rowHeight
                width: spacerWidth
                color: "transparent"
            }
            Button {
                text: "Lockdown"
                id: lockButton
                onClicked: { controller.lockdown(); locked = ! locked}
                property bool locked : false
            }
            Rectangle {
                height: rowHeight
                width: spacerWidth
                color: "transparent"
            }
            Button {
                id: haltButton
                text: "Halt"
                onClicked: { controller.halt(); halted = ! halted }
                property bool halted : false
            }
            Text {
                height: rowHeight
                width: implicitWidth + rowHeight/4
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                text: "man.:"
            }

            ComboBox {
                model: ["Isolieren (eng)", "Isolieren(weit)", "Tracken (eng)", "Tracken (weit)", "Infizieren (max. 1)", "Infizieren (max. 5)", "Impfen (eng)", "Impfen (weit)" ]
                Component.onCompleted: currentIndex = 2;
                height: rowHeight
                width: implicitWidth + 10
                editable: false
                onCurrentTextChanged: {
                    controller.interactionType = currentIndex
                }
            }
            Text {
                height: rowHeight
                width: implicitWidth + rowHeight/4
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                text: "auto:"
            }

            ComboBox {
                model: [ "  -  ", "Isolieren(verzögert)", "Isolieren (sofort)", "Tracken (verzögert)",  "Tracken (sofort)", "Zentrum Isolieren (zögernd)",  "Zentrum Isolieren (normal)", "Zentrum Isolieren (sofort)" ]
                Component.onCompleted: currentIndex = 0;
                height: rowHeight
                width: implicitWidth + 10
                editable: false
                onCurrentTextChanged: {
                    controller.automationType = currentIndex
                }
            }
            Text {
                height: rowHeight
                width: implicitWidth + rowHeight/4
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                text: " Zeitlupe:"
            }

            Slider {
                id: msBetweenSlider1
                height: rowHeight
                width: settingsColumn.col1Width
                from: 10
                to: 200
                value: controller ? controller.msBetweenSteps : 60
                onValueChanged: {
                    if (controller) controller.msBetweenSteps = value
                }
            }
            CheckBox {
                height: rowHeight
                text: "Erstinfektion"
                checked: controller ? controller.initialInfection : true;
                onCheckedChanged: if (controller) controller.initialInfection = checked


            }

            Button {
                id: settingsButton
                height: rowHeight
                width: implicitWidth
                text: "Einstellungen"
                onClicked: settingsPopup.open()
            }
            Rectangle {
                visible: controller ? controller.overloadState : false
                id: overloadSign
                height: rowHeight;
                width: overloadText.contentWidth + height/2
                color: "red"
                Text {
                    id: overloadText
                    text: "Überlastung!"
                    anchors.fill: parent
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    color: "yellow"
                    font.bold: true
                }
            }
        }
        Rectangle {
            width: parent.width
            height: parent.height - topRow.height-65;
            color: "white"
            RenderPaintedItem {
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        controller.clicked(mouseX, mouseY)
                    }
                }
                id: renderPaintedItem
                objectName: "renderPaintedItem"
                width: parent.width
                height: parent.height
            }
        }
        Rectangle {
            width: parent.width
            height: 65
            color: "white"
            DiagramPaintedItem {
                id: diagramPaintedItem
                objectName: "diagramPaintedItem"
                width: parent.width
                height: parent.height
            }
        }
    }

    Rectangle {
        id: haltedWarnRect
        x: parent.width - width
        height: rowHeight
        width: haltedText.implicitWidth
        visible: haltButton.halted
        Text {
            anchors.centerIn: parent
            id: haltedText
            text: qsTr(" - HALTED - ")
            color: "white"
            font.bold: true
        }
        color: "red"
    }
    Rectangle {
        x: parent.width - (width + (haltedWarnRect.visible ? haltedWarnRect.width : 0 ))
        height: rowHeight
        width: lockText.implicitWidth
        visible: lockButton.locked
        Text {
            anchors.centerIn: parent
            id: lockText
            text: qsTr(" - Lockdown - ")
            color: "black"
            font.bold: true
        }
        color: "yellow"
    }
//    Rectangle {
//        z: 100
//        id: result
//        color: "white"
//        border.color: "black"
//        border.width: 5
//        anchors.centerIn: parent
//        width: resultText.implicitWidth + 20
//        height: resultText.implicitHeight + 20
//        property real deaths: controller? 100 * controller.percentFinallyDeceased : -1
//        Text {
//            id: resultText
//            anchors.centerIn: parent
////            text: result.deaths.toFixed(1) + " % of your\npopulation is deceased";
//            text: result.deaths.toFixed(1) + " % der Population\nist verstorben";
//            font.bold: true
//            font.pixelSize: 20
//            horizontalAlignment: Text.AlignHCenter
//        }
//        onDeathsChanged: {
//            if (deaths >= 0) {
//                result.visible = true
//            } else {
//                result.visible = false
//            }
//        }
//    }
// -------------------------------------------------------------------------------------------
    Popup {
        id: settingsPopup
        width: mainWindow.width
        height: mainWindow.height

        onClosed: if (controller) controller.restart()
        Column {
            width: settingsPopup.width
            height: settingsPopup.height
            id: settingsColumn
            property int col1Width: 150
            property int col2Width: 350
            spacing: 2
            SettingsTitle {
                text: qsTr("Allgemein")
            }
            Row {
                height: rowHeight
                width: settingsColumn.width

                ComboBox {
                    id: personCountCombo
                    function findIndex() {
                        if (controller) {
                                    for (var i = 0; i< model.length; i++) {
                                        if (model[i] === controller.populationCount) return i
                                    }
                                }
                        return -1
                    }
                    model: [1000, 2000, 2500, 3000, 3500, 4000, 5000, 6000, 7000 , 50000]
                    editable: true
                    width: settingsColumn.col1Width
                    height: rowHeight

                    // Attention: fragile interdependend implementation, don't change ordering details
                    Component.onCompleted: {
                        personCountCombo.contentItem.color = "black"
                        if (findIndex() >= 0) { currentIndex = findIndex() }   // common value
                        else { editText = controller.populationCount }         // user defined
                    }
                    onEditTextChanged: {
                        if(acceptableInput) { personCountCombo.contentItem.color = "black" }
                        else { personCountCombo.contentItem.color = "red" }
                    }
                    onCurrentIndexChanged: {
                        if (controller && currentIndex >= 0) {
                            personCountCombo.contentItem.color = "black"
                            controller.populationCount = model[currentIndex]
                        }
                    }
                    onAccepted: {
                        if (controller) { controller.populationCount = editText }
                    }
                    validator: IntValidator { bottom: 100; top: 1000000;}
                }

                TextArea {
                    width: settingsColumn.col2Width-recommendedButton.width
                    height: rowHeight
                    text: "Bevölkerungsgröße"
                    verticalAlignment: "AlignVCenter"
                    horizontalAlignment: "AlignLeft"
                    background: Rectangle {
                        border.color: "black"
                        border.width: 1
                    }
                }
                Button {
                    id: recommendedButton
                    height: rowHeight
                    text: "Standard"
                    onClicked: {
                        var number = Screen.width *  Screen.height / 576
                        personCountCombo.editText = number
                        if (controller) { controller.populationCount = number }
                    }
                }
            }
            Row {
                height: rowHeight
                width: settingsColumn.width
                Slider {
                    id: infectionConstantSlider
                    height: rowHeight
                    width: settingsColumn.col1Width
                    from: 0
                    to: 100
                    value: controller ? controller.infectionConstantSliderValue : 100
                    onValueChanged: {
                        controller.infectionConstantSliderValue = value
                    }
                }
                TextArea {
                    width: settingsColumn.col2Width
                    height: rowHeight
                    text: "Ansteckungskonstante ("+Math.round(infectionConstantSlider.value)+")"
                    verticalAlignment: "AlignVCenter"
                    horizontalAlignment: "AlignLeft"
                    background: Rectangle {
                        border.color: "black"
                        border.width: 1
                    }
                }
            }
            Row {
                height: rowHeight
                width: settingsColumn.width
                Slider {
                    id: preVaccinationSlider
                    height: rowHeight
                    width: settingsColumn.col1Width
                    from: 0
                    to: 100
                    stepSize: 0.5
                    value: controller ? controller.preVaccinated : 0
                    onValueChanged: {
                        controller.preVaccinated = value
                    }
                }
                TextArea {
                    width: settingsColumn.col2Width
                    height: rowHeight
                    text: "Vorab-Impfungen ("+Math.round(preVaccinationSlider.value)+"%)"
                    verticalAlignment: "AlignVCenter"
                    horizontalAlignment: "AlignLeft"
                    background: Rectangle {
                        border.color: "black"
                        border.width: 1
                    }
                }
            }
            Row {
                height: rowHeight
                width: settingsColumn.width
                Slider {
                    id: msBetweenSlider
                    height: rowHeight
                    width: settingsColumn.col1Width
                    from: 10
                    to: 200
                    value: controller ? controller.msBetweenSteps : 60
                    onValueChanged: {
                        if (controller) controller.msBetweenSteps = value
                    }
                }
                TextArea {
                    width: settingsColumn.col2Width
                    height: rowHeight
                    text: "Zeit zwischen Berechnungsschritten ("+Math.round(msBetweenSlider.value)+" ms)"
                    verticalAlignment: "AlignVCenter"
                    horizontalAlignment: "AlignLeft"
                    background: Rectangle {
                        border.color: "black"
                        border.width: 1
                    }
                }
            }
            SettingsTitle {
                text: qsTr("Nutzung einer Tracking-App")
            }
            Row {
                height: rowHeight
                width: settingsColumn.width
                Slider {
                    id: percentTracingAppSlider
                    height: rowHeight
                    width: settingsColumn.col1Width
                    from: 0
                    to: 100
                    value: controller ? controller.percentOfTracingApps : 100
                    onValueChanged: {
                        if (controller) controller.percentOfTracingApps = value
                    }
                }
                TextArea {
                    width: settingsColumn.col2Width
                    height: rowHeight
                    text: "Nutzung der App ("+Math.round(percentTracingAppSlider.value)+"% der Bevölkerung)"
                    verticalAlignment: "AlignVCenter"
                    horizontalAlignment: "AlignLeft"
                    background: Rectangle {
                        border.color: "black"
                        border.width: 1
                    }
                }

            }

            Row {
                height: rowHeight
                width: settingsColumn.width
                Slider {
                    id: appAccuracySlider
                    height: rowHeight
                    width: settingsColumn.col1Width
                    from: 0
                    to: 100
                    value: controller ? controller.tracingAppAccuracy : 100
                    onValueChanged: {
                        if (controller) controller.tracingAppAccuracy = value
                    }
                }
                TextArea {
                    width: settingsColumn.col2Width
                    height: rowHeight
                    text: "Die App erfasst pro Messung "+Math.round(appAccuracySlider.value)+"% der Nachbarn"
                    verticalAlignment: "AlignVCenter"
                    horizontalAlignment: "AlignLeft"
                    background: Rectangle {
                        border.color: "black"
                        border.width: 1
                    }
                }
            }
            SettingsTitle {
                text: qsTr("Bewegungsmuster")
            }
            Row {
                height: rowHeight
                width: settingsColumn.width
                ComboBox {
                    id: globalMotionTypeSlider
                    model: ["Freier Zufall", "Attraktorbasiert" ]
                    Component.onCompleted: if (controller) currentIndex = controller.globalMotionType;
                    height: rowHeight
                    width: implicitWidth + 10
                    currentIndex: 1
                    editable: false
                    onCurrentTextChanged: {
                        if (controller) controller.globalMotionType = currentIndex
                    }
                }
                TextArea {
                    width: settingsColumn.col2Width
                    height: rowHeight
                    text: "Freie Bewegung vs. Rudelbildung"
                    verticalAlignment: "AlignVCenter"
                    horizontalAlignment: "AlignLeft"
                    background: Rectangle {
                        border.color: "black"
                        border.width: 1
                    }
                }
            }
            Row {
                height: rowHeight
                width: settingsColumn.width
                visible: globalMotionTypeSlider.currentIndex === 1
                Slider {
                    id: numberOfAttractorsSlider
                    height: rowHeight
                    width: settingsColumn.col1Width
                    from: 2
                    to: 100
                    stepSize: 1
                    value: controller ? controller.numberOfAttractors : 60
                    onValueChanged: {
                        if (controller) controller.numberOfAttractors = value
                    }
                }
                TextArea {
                    width: settingsColumn.col2Width
                    height: rowHeight
                    text: "Anzahl von Attraktoren: "+numberOfAttractorsSlider.value
                    verticalAlignment: "AlignVCenter"
                    horizontalAlignment: "AlignLeft"
                    background: Rectangle {
                        border.color: "black"
                        border.width: 1
                    }
                }
            }
            Row {
                height: rowHeight
                width: settingsColumn.width
                visible: globalMotionTypeSlider.currentIndex === 1
                Slider {
                    id: weightCouchPotatoesSlider
                    height: rowHeight
                    width: settingsColumn.col1Width
                    from: 0
                    to: 100
                    value: controller ? controller.weightCouchPotatoes : 100
                    onValueChanged: {
                        if (controller) controller.weightCouchPotatoes = value
                    }
                }
                TextArea {
                    width: settingsColumn.col2Width
                    height: rowHeight
                    text: "Gewichtung Eremiten: "+Math.round(weightCouchPotatoesSlider.value)
                    verticalAlignment: "AlignVCenter"
                    horizontalAlignment: "AlignLeft"
                    background: Rectangle {
                        border.color: "black"
                        border.width: 1
                    }
                }
            }
            Row {
                height: rowHeight
                width: settingsColumn.width
                visible: globalMotionTypeSlider.currentIndex === 1
                Slider {
                    id: weightLocallyAttractedSlider
                    height: rowHeight
                    width: settingsColumn.col1Width
                    from: 0
                    to: 100
                    value: controller ? controller.weightLocallyAttracted : 100
                    onValueChanged: {
                        if (controller) controller.weightLocallyAttracted = value
                    }
                }
                TextArea {
                    width: settingsColumn.col2Width
                    height: rowHeight
                    text: "Gewichtung Lokalmatadore: "+Math.round(weightLocallyAttractedSlider.value)
                    verticalAlignment: "AlignVCenter"
                    horizontalAlignment: "AlignLeft"
                    background: Rectangle {
                        border.color: "black"
                        border.width: 1
                    }
                }
            }
            Row {
                height: rowHeight
                width: settingsColumn.width
                visible: globalMotionTypeSlider.currentIndex === 1
                Slider {
                    id: weightCommutersSlider
                    height: rowHeight
                    width: settingsColumn.col1Width
                    from: 0
                    to: 100
                    value: controller ? controller.weightCommuters : 100
                    onValueChanged: {
                        if (controller) controller.weightCommuters = value
                    }
                }
                TextArea {
                    width: settingsColumn.col2Width
                    height: rowHeight
                    text: "Gewichtung Pendler: "+Math.round(weightCommutersSlider.value)
                    verticalAlignment: "AlignVCenter"
                    horizontalAlignment: "AlignLeft"
                    background: Rectangle {
                        border.color: "black"
                        border.width: 1
                    }
                }
            }
            Row {
                height: rowHeight
                width: settingsColumn.width
                visible: globalMotionTypeSlider.currentIndex === 1
                Slider {
                    id: weightTravellersSlider
                    height: rowHeight
                    width: settingsColumn.col1Width
                    from: 0
                    to: 100
                    value: controller ? controller.weightTravellers : 100
                    onValueChanged: {
                        if (controller) controller.weightTravellers = value
                    }
                }
                TextArea {
                    width: settingsColumn.col2Width
                    height: rowHeight
                    text: "Gewichtung Reisende: "+Math.round(weightTravellersSlider.value)
                    verticalAlignment: "AlignVCenter"
                    horizontalAlignment: "AlignLeft"
                    background: Rectangle {
                        border.color: "black"
                        border.width: 1
                    }
                }
            }





        }
    }
    Dialog {
        id: aboutDialog
        title: qsTr("About")
        width: mainWindow.width * 2 / 3
        height: mainWindow.height * 2 / 3
        x: mainWindow.width / 6
        y: mainWindow.height/ 6
        standardButtons: Dialog.Ok
        footer: DialogButtonBox {
            standardButtons: Dialog.Ok
        }

        ScrollView {
            id: scrollViewDialog
            clip: true
            objectName: "scrollViewDialog"
            ScrollBar.vertical.policy: ScrollBar.AsNeeded
            ScrollBar.horizontal.policy: ScrollBar.AsNeeded


            width: aboutDialog.width;
            height: aboutDialog.height;
            contentHeight: dialogColumn.childrenRect.height + 200
            Rectangle {
                color: "transparent"
                width:  parent.parent.parent.width
                height: childrenRect.height

                Column {
                    id: dialogColumn
                    width: parent.width
                    Text {
                        wrapMode: Text.Wrap
                        width: scrollViewDialog.width - 15
                        height: contentHeight
                        text: "<h3>"+qsTr("Über DynamicInfectionSimulation")+"</h3>"
                    }
                    Text{
                        wrapMode: Text.Wrap
                        width: scrollViewDialog.width - 15
                        height: contentHeight
//                        property string programVersion: typeof controller === "undefined" ? "" : controller.programVersion()
                        text: "<p>"+qsTr("(c) 2020 by Sebastian Diel, Version 1.0")+"</p>"
                    }
                    Text{
                        wrapMode: Text.Wrap
                        width: scrollViewDialog.width - 15
                        height: contentHeight
                        text: "<p>"+qsTr("Diese Software soll ein Verständnis der Mechanismen ermöglichen, welche einer Epidemie / Pandemie zu Grunde liegen.")+"</p>"
                    }
                    Text {
                        wrapMode: Text.Wrap
                        width: scrollViewDialog.width - 15
                        height: contentHeight
                        text: "<h3>"+qsTr("About Qt")+"</h3>"
                    }
                    Text{
                        wrapMode: Text.Wrap

                        width: scrollViewDialog.width - 15
                        height: contentHeight
                        property string qtVersion: typeof controller === "undefined" ? "" : (controller ? controller.qtVersion : "?")
                        text: "<p>"+qsTr("This program uses Qt version ")+qtVersion+".</p>"
                    }
                    Text{
                        wrapMode: Text.Wrap
                        width: scrollViewDialog.width - 15
                        height: contentHeight
                        text: "<p>"+qsTr("Qt is a C++ toolkit for cross-platform application "
                                         +"development.")+"</p>"
                              +"<p>"+qsTr("Qt provides single-source portability across all major desktop "
                                          +"operating systems. It is also available for embedded Linux and other "
                                          +"embedded and mobile operating systems.")+"</p>"
                              +"<p>"+qsTr("Qt is available under three different licensing options designed "
                                          +"to accommodate the needs of our various users.")+"</p>"
                              +"<p>"+qsTr("Qt licensed under our commercial license agreement is appropriate "
                                          +"for development of proprietary/commercial software where you do not "
                                          +"want to share any source code with third parties or otherwise cannot "
                                          +"comply with the terms of the GNU LGPL version 3 or GNU LGPL version 2.1.")+"</p>"
                              +"<p>"+qsTr("Qt licensed under the GNU LGPL version 3 is appropriate for the "
                                          +"development of Qt&nbsp;applications provided you can comply with the terms "
                                          +"and conditions of the GNU LGPL version 3.")+"</p>"
                              +"<p>"+qsTr("Qt licensed under the GNU LGPL version 2.1 is appropriate for the "
                                          +"development of Qt&nbsp;applications provided you can comply with the terms "
                                          +"and conditions of the GNU LGPL version 2.1.")+"</p>"
                              +"<p>"+qsTr("Please see ")+"<a href=\"http://qt.io/licensing/\">qt.io/licensing</a>"
                              +qsTr(" for an overview of Qt licensing.")+"</p>"
                              +"<p>"+qsTr("Copyright (C) 2015 The Qt Company Ltd and other "
                                          +"contributors.")+"</p>"
                              +"<p>"+qsTr("Qt and the Qt logo are trademarks of The Qt Company Ltd.")+"</p>"
                              +"<p>"+qsTr("Qt is The Qt Company Ltd product developed as an open source "
                                          +"project. See <a href=\"http://qt.io/\">qt.io</a> for more information.")+"</p>"
                        onLinkActivated: {
                            Qt.openUrlExternally(link)
                        }
                    }

                }

            }
        }
    }
}
