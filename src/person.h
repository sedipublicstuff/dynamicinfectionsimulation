/*
 * Copyright (C) 2020 Sebastian Diel
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>
 */

#ifndef PERSON_H
#define PERSON_H

#include <QObject>
#include <QPointF>
#include "math.h"
#include <QDebug>
#include <QQueue>

class Person: public QObject
{
    Q_OBJECT

    Q_PROPERTY(QPointF pos READ pos WRITE setPos NOTIFY posChanged)
    Q_PROPERTY(qreal speed READ speed WRITE setSpeed NOTIFY speedChanged)
    Q_PROPERTY(qreal direction READ direction WRITE setDirection NOTIFY directionChanged)
    Q_PROPERTY(Person::HealthType healthState READ healthState WRITE setHealthState NOTIFY healthStateChanged)

signals:
    void posChanged();
    void speedChanged();
    void directionChanged();
    void healthStateChanged();

public:
    enum class HealthType {
        SUSCEPTIBLE = 0,
        INFECTED_ASYMPT = 1,
        INFECTED_LIGHT = 2,
        INFECTED_HEAVY = 3,
        RECOVERED = 4,
        DECEASED = 5,
        VACCINATED = 6,
    };
    Q_ENUM(HealthType)

    enum class MotionType {
        RANDOM = 0,
        COUCH_POTATOE = 1,      // Has two adjacent attractors, visits second seldomly, moves just a bit
        LOCALLY_ATTRACTED = 2,  // Has two adjacent attractors, visits second oftenly, moves around)
        COMMUTER = 3,           // Has two distant attractors, alternates frequently
        TRAVELLER = 4           // can visit visit every attractor
    };
    Q_ENUM(MotionType)






    Person();

    QPointF pos() const;
    void setPos(const QPointF &pos);

    qreal speed() const;
    void setSpeed(const qreal &speed);

    qreal direction() const;
    void setDirection(const qreal &direction);

    HealthType healthState() const;
    HealthType visibleHealthState() const;
    void setHealthState(const HealthType &healthState);

    void infect();
    void vaccinate();

    static unsigned int s_seed;
    static inline int fastrand() {
      s_seed = (214013*s_seed+2531011);
      return (s_seed>>16)&0x7FFF;
    }

    static qreal randomSpeed(MotionType motionType = MotionType::RANDOM) {
        qreal speedConst = 0;
        switch (motionType) {
            case MotionType::RANDOM: speedConst = 3.3; break;
            case MotionType::COMMUTER: speedConst = 5.1; break;
            case MotionType::TRAVELLER: speedConst = 8.1; break;
            case MotionType::COUCH_POTATOE: speedConst = 0.1; break;
            case MotionType::LOCALLY_ATTRACTED: speedConst = 3.3; break;
        }
        return fmod(fastrand(),speedConst*100.0)/100.0;
    }


    static qreal randomDirection() { return fmod(fastrand(),2* M_PI); }

    static qreal randomBiasedDirection(qreal angle) {
        return angle+(0.5-(fastrand()%32/31.0));
//    good enough, could perhaps be weighed by uaing a cosine function along the lines of
//        qreal t = 0.5-(fastrand()%128/127.0);
//        return angle += t > 0 ?  (1-cos(2*t)) : -(1-cos(2*t));
//        //still wrong though)
    }

    qreal angleToPoint(QPointF p) {
        return atan2(p.y()-m_pos.y(), p.x()-m_pos.x());
    }
    static qreal angle(QPointF from, QPointF to) {
        return atan2(to.y()-from.y(), to.x()-from.x());
    }
    void refineSpeedAndDirection() {
        switch (m_motionType) {
            case MotionType::RANDOM:
                m_speed = this->randomSpeed();
                m_direction = this->randomDirection();
                break;
        case MotionType::COMMUTER:
        case MotionType::TRAVELLER:
            if (!m_attractors.isEmpty()) {
                m_speed = this->randomSpeed(m_motionType);
                if (m_directedTowardsAttractor) {
                    m_direction = this->randomBiasedDirection(this->angleToPoint(m_attractors.first()));
                } else {
                    // swap / cycle through attractors
                    m_attractors.prepend(m_attractors.last());
                    m_attractors.pop_back();
                    m_direction = this->randomBiasedDirection(this->angleToPoint(m_attractors.first())) + M_PI;
                    m_directedTowardsAttractor = true;
                }

            }
            break;
        case MotionType::COUCH_POTATOE:
            break;
        case MotionType::LOCALLY_ATTRACTED:
            if (!m_attractors.isEmpty()) {
                m_speed = this->randomSpeed(m_motionType);
                if (m_directedTowardsAttractor) {
                    m_direction = this->randomBiasedDirection(this->angleToPoint(m_attractors.first()));
                } else {
                    m_direction = this->randomBiasedDirection(this->angleToPoint(m_attractors.first())) + M_PI;
                }

            }
            break;
        }
    }


    QPointF advanceInSpace(qreal maxX, qreal maxY);
    void advanceInTime(QVector<Person*> neighbours);



    bool willDie() const;
    void setWillDie(bool willDie);

    bool inIsolation() const;
    void setInIsolation(bool inIsolation);
    void clearNeigbourHistory() {m_neighbourHistory.clear(); }

    QQueue<QVector<Person *> > neighbourHistory() const;
    void setNeighbourHistory(const QQueue<QVector<Person *> > &neighbourHistory);

    qreal infectionConstant() const;
    void setInfectionConstant(const qreal &infectionConstant);

    bool usesTrackingApp() const;
    void setUsesTrackingApp(bool usesTrackingApp);

    int timeSinceLastHealthChange() {
        if (m_timeSinceInfection < 0 ) return -1;
        return m_timeSinceInfection - m_infectionHistory.lastKey();
    }

    MotionType motionType() const;
    void setMotionType(const MotionType &motionType);

    QVector<QPointF> attractors() const;
    void setAttractors(const QVector<QPointF> &attractors);

    bool directedTowardsAttractor() const;
    void setDirectedTowardsAttractor(bool directedTowardsAttractor);

private:
    QPointF m_pos = QPointF(0,0);
    qreal m_speed = 0;
    qreal m_direction = 0;
    HealthType m_healthState = Person::HealthType::SUSCEPTIBLE;
    long m_timeSinceInfection = -1;
    qreal m_infectionConstant = 0.26;
    QMap<long, HealthType> m_infectionHistory;
    bool m_willShowSymptoms = false;
    bool m_willBecomeHeavy = false;
    bool m_willDie = false;


    int m_timeToSymptoms = 60;
    int m_timeToRecoveryFromLight = 120;
    int m_timeToRecoveryFromHeavy = 180;
    int m_timeToVisibility = 80;
    int m_timeToDeath = 140;

    int m_isolationTime = m_timeToSymptoms*2;
    int m_isolationCounter = m_isolationTime;
    bool m_inIsolation = false;

    bool m_usesTrackingApp = true;

    QQueue<QVector<Person*>> m_neighbourHistory;

    QVector<QPointF> m_attractors;
    QPointF m_currentAttractor;
    bool m_directedTowardsAttractor = fastrand()%100 > 50;
    MotionType m_motionType = MotionType::RANDOM;

};

Q_DECLARE_METATYPE(Person*);
Q_DECLARE_METATYPE(Person::HealthType);


#endif // PERSON_H
