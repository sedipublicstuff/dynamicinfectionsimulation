/*
 * Copyright (C) 2020 Sebastian Diel
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>
 */

#include "controller.h"
#include <QDebug>
#include <QQmlContext>

#include <QQmlEngine>
#include <QTimer>
#include "math.h"
#include <QQmlApplicationEngine>
#include "renderpainteditem.h"
#include <QElapsedTimer>
#include "temporaryshape.h"

#include <cstdlib>
#include <random>
#include <QDateTime>
#include <QScreen>

Controller::Controller(QQmlApplicationEngine *engine, QGuiApplication *app)
    :QObject ()
{
    srand(uint(QDateTime::currentDateTime().toMSecsSinceEpoch()));
    m_engine = engine;
    m_app = app;
    m_population = new Population();
    qmlRegisterUncreatableType<Person>("de.containment.herrdiel", 1, 0, "Person",QString("Not meant to be created in QML"));
    QQuickView view(engine, nullptr);
    QQmlContext *ctxt = view.rootContext();
    ctxt->setContextProperty("controller", this);
    this->advance();
}

void Controller::init()
{
    Q_ASSERT(m_engine);
    m_renderPaintedItem = findQmlElement<RenderPaintedItem*>(m_engine->rootObjects().at(0),"renderPaintedItem");
    Q_ASSERT(m_renderPaintedItem);
    m_diagramPaintedItem = findQmlElement<DiagramPaintedItem*>(m_engine->rootObjects().at(0),"diagramPaintedItem");
    Q_ASSERT(m_diagramPaintedItem);
    connect(m_diagramPaintedItem, SIGNAL(infectionsDownToZero()), this, SLOT(infectionsDownToZeroSlot()));

    qmlRegisterType<RenderPaintedItem>("de.containment.herrdiel", 1, 0, "RenderPaintedItem");

    auto size = m_app->primaryScreen()->size();
    this->setPopulationCount(int(size.width() * size.height()) / 576);

    m_renderPaintedItem->setPopulation(m_population);
    m_renderPaintedItem->setDotRadius(m_dotRadius);

    this->populatePopulation();

    m_renderPaintedItem->repaint();
}

Population *Controller::population() const
{
    return m_population;
}

void Controller::setPopulation(Population *population)
{
    m_population = population;
}

// needed for using QPointF as a QHash key
inline uint qHash (const QPointF & key)
{
    return qHash (QPair<qreal,qreal>(key.x(), key.y()) );
}

void Controller::advance()
{
    if (m_halted) return;
    QElapsedTimer t;
    t.start();
    if (m_renderPaintedItem) {

        qreal itemWidth = m_renderPaintedItem->width();
        qreal itemHeight = m_renderPaintedItem->height();

        m_raster.clear();

        m_raster = QVector<QVector<QVector<Person*>>> (m_rasterRows, QVector<QVector<Person*>>(m_rasterCols, QVector<Person*>()));
        m_rasterCellWidth = itemWidth / m_rasterCols;
        m_rasterCellHeight = itemHeight / m_rasterRows;

        // change a few speeds/directions


        int jumpStepReference = m_globalMotionType == GlobalMotionType::MOVE_RANDOMLY ? 30 : 200;

        int i = int(fmod(fastrand(), m_population->persons().count()/jumpStepReference));
        while (i >= 0 &&  i < m_population->persons().count()) {
            m_population->personAt(i)->refineSpeedAndDirection();
            m_population->persons().at(i)->setDirectedTowardsAttractor(true);
            i += int(fmod(fastrand(), m_population->rowCount()/jumpStepReference));
        }


        // move person on screen
        for (auto p : m_population->persons()) {
            if (!m_lockdown) {
                p->advanceInSpace(itemWidth, itemHeight);
            }
            // raster is later being used for efficient collision detection
            this->insertIntoRaster(p, itemWidth, itemHeight);

            if (!p->attractors().isEmpty()) {
                if (fabs(p->pos().x()-p->attractors().first().x()) < m_dotRadius
                        &&
                        fabs(p->pos().y()-p->attractors().first().y())< m_dotRadius)
                {
                    p->setDirectedTowardsAttractor(false);
                }
            }

        }


        QHash<QPointF, int> infectedAttractors;
        for (auto g : m_globalAttractors) {
            infectedAttractors[g] = 0;
        }
        QPointF currentAttractor = QPointF();

        for (auto p : m_population->persons()) {

             // AUTOMATION EFFECTS
            if (p->healthState() == Person::HealthType::INFECTED_LIGHT || p->healthState() == Person::HealthType::INFECTED_HEAVY ) {
                switch (m_automationType) {
                    case AutomationType::ISOLATE_IMMEDIATELY_AUTO:
                         p->setInIsolation(true);
                        break;
                    case AutomationType::ISOLATE_DELAYED_AUTO:
                    if (p->timeSinceLastHealthChange() > 25) { // 0-30
                        p->setInIsolation(true);
                    }
                        break;
                    case AutomationType::TRACK_IMMEDIATELY_AUTO:
                        this->tracePerson(p);
                        break;
                    case AutomationType::TRACK_DELAYED_AUTO:
                        if (p->timeSinceLastHealthChange() > 25) { // 0-30
                            this->tracePerson(p);
                        }
                        break;

                    case AutomationType::ISOLATE_ATTRACTOR_OUTRIGHT:
                    case AutomationType::ISOLATE_ATTRACTOR_NORMAL:
                    case AutomationType::ISOLATE_ATTRACTOR_DILATORY:
                        currentAttractor =  nearestAttractor(p->pos().x(), p->pos().y());
                        infectedAttractors.insert(currentAttractor, infectedAttractors.value(currentAttractor)+1);
                    break;


                case AutomationType::NO_AUTO: break;
                }
            }

            p->advanceInTime(neighbours(p, 10, itemWidth, itemHeight));
        }

        int attractorInfectionsThreshold;
        switch (m_automationType) {
            case AutomationType::ISOLATE_ATTRACTOR_OUTRIGHT: attractorInfectionsThreshold = 0; break;
            case AutomationType::ISOLATE_ATTRACTOR_NORMAL: attractorInfectionsThreshold = 1; break;
            case AutomationType::ISOLATE_ATTRACTOR_DILATORY: attractorInfectionsThreshold = 2; break;
            default: attractorInfectionsThreshold = 99; break;
        }

        QHashIterator<QPointF, int> iter(infectedAttractors);
        while (iter.hasNext()) {
            iter.next();
            if (iter.value() > attractorInfectionsThreshold) {
                isolateAttractor(iter.key().x(), iter.key().y(),100);
            }
        }



        m_renderPaintedItem->repaint();
        this->sendSetToDiagram();
        m_diagramPaintedItem->repaint();
    }

    int waitTime = m_msBetweenSteps - int(t.elapsed());
    this->setOverloadState(waitTime<5);
    if (waitTime < 5) waitTime = 5;

    QTimer::singleShot(waitTime, this, SLOT(advance()));
}

void Controller::interactionIsolate(qreal x, qreal y, qreal radius)
{
    auto list = personsWithinDistanceFrom(x, y, radius);
    for (auto p : list) {
        if (p->healthState() != Person::HealthType::DECEASED && p->healthState() != Person::HealthType::RECOVERED){
            p->setInIsolation(true);
        }
    }
}

void Controller::interactionVaccinate(qreal x, qreal y, qreal radius)
{
    auto list = personsWithinDistanceFrom(x, y, radius);
    for (auto p : list) {
        p->vaccinate();
    }
}

void Controller::isolateAttractor(qreal x, qreal y, qreal radius)
{
    if (m_globalAttractors.isEmpty()) return;
    QMap<qreal, QPointF> distanceMap;
    QPointF p(x,y);
    for (auto g : m_globalAttractors) {
        distanceMap.insert(this->distanceSquared(p, g), g);
    }
    auto list = personsWithinDistanceFrom(distanceMap.first().x(), distanceMap.first().y(),radius);
    for (auto p : list) {
        p->setInIsolation(true);
    }

}

QPointF Controller::nearestAttractor(qreal x, qreal y)
{
    if (m_globalAttractors.isEmpty()) return QPointF();
    QMap<qreal, QPointF> distanceMap;
    QPointF p(x,y);
    for (auto g : m_globalAttractors) {
        distanceMap.insert(this->distanceSquared(p, g), g);
    }
    return distanceMap.first();
}

void Controller::tracePerson(Person* p) {
    p->setInIsolation(true);
    // does this person use an app?
    if (fastrand()%100 <= m_percentOfTracingApps-1){
        for (auto neighbours : p->neighbourHistory()) {
            for (auto n : neighbours) {
                if (m_tracingAppAccuracy < 100) {
                    // in some cases we skip a traced person
                    if (fastrand()%100 > m_tracingAppAccuracy-1) {
                        continue;
                    }
                }
                if (!n->inIsolation()) {
                    n->setInIsolation(true);
                    TemporaryShape* line = new TemporaryShape(
                                TemporaryShape::ShapeType::LINE,
                                p->pos().x(),
                                p->pos().y(),
                                n->pos().x(),
                                n->pos().y());
                    line->setLineWidth(3);
                    line->setColor(QColor("#ffAA00"));
                    m_renderPaintedItem->appendToTemporaryShapes(line);
                }
            }
        }
    }
}


//   ISOLATE WITH APP
void Controller::interactionTrace(qreal x, qreal y, qreal radius)
{
    auto list = personsWithinDistanceFrom(x, y, radius);
    TemporaryShape* circle = new TemporaryShape(TemporaryShape::ShapeType::ELLIPSE, x-radius/2, y-radius/2, radius,radius);
    circle->setLineWidth(1);
    circle->setColor(QColor(Qt::black));
    m_renderPaintedItem->appendToTemporaryShapes(circle);
    for (auto p : list) {
        if (p->healthState() != Person::HealthType::DECEASED && p->healthState() != Person::HealthType::RECOVERED){
            tracePerson(p);
        }
    }
}

void Controller::interactionInfect(qreal x, qreal y, qreal radius, int maxInfect)
{
    auto list = personsWithinDistanceFrom(x, y, radius);
    QMap<qreal, Person*> distanceSquareMap;
    for (auto p: list) {
        if (p->healthState() == Person::HealthType::SUSCEPTIBLE) {
            distanceSquareMap.insert((p->pos().x()-x) * (p->pos().x()-x) + (p->pos().y()-y) * (p->pos().y()-y), p);
        }
    }
    QMapIterator<qreal, Person*> iter(distanceSquareMap);
    while (iter.hasNext() && maxInfect > 0) {
        iter.next().value()->infect();
        maxInfect --;
    }
}

qreal Controller::sliderValueToInfectionConstant(int sliderValue)
{
    return 0.35 + 6 * sliderValue / 100;
}

qreal Controller::preVaccinated() const
{
    return m_preVaccinated;
}

void Controller::setPreVaccinated(const qreal &preVaccinated)
{
    if (fabs(preVaccinated-m_preVaccinated) >= 0.01) {
        m_preVaccinated = preVaccinated;
        emit this->preVaccinatedChanged();
    }
}

QString Controller::qtVersion() const
{
    return m_qtVersion;
}

void Controller::setQtVersion(const QString &qtVersion)
{
    m_qtVersion = qtVersion;
}

bool Controller::overloadState() const
{
    return m_overloadState;
}

void Controller::setOverloadState(bool overloadState)
{
    if (overloadState != m_overloadState){
        m_overloadState = overloadState;
        emit this->overloadStateChanged();
    }
}

int Controller::numberOfAttractors() const
{
    return m_numberOfAttractors;
}

void Controller::setNumberOfAttractors(int numberOfAttractors)
{
    if (numberOfAttractors != m_numberOfAttractors) {
        m_numberOfAttractors = numberOfAttractors;
        emit this->numberOfAttractorsChanged();
    }
}

int Controller::weightCouchPotatoes() const
{
    return m_weightCouchPotatoes;
}

void Controller::setWeightCouchPotatoes(int weightCouchPotatoes)
{
    if (weightCouchPotatoes != m_weightCouchPotatoes) {
        m_weightCouchPotatoes = weightCouchPotatoes;
        emit this->weightCouchPotatoesChanged();
    }
}

int Controller::weightTravellers() const
{
    return m_weightTravellers;
}

void Controller::setWeightTravellers(int weightTravellers)
{
    if (weightTravellers != m_weightTravellers) {
        m_weightTravellers = weightTravellers;
        emit this->weightTravellersChanged();
    }
}

int Controller::weightCommuters() const
{
    return m_weightCommuters;
}

void Controller::setWeightCommuters(int weightCommuters)
{
    if (weightCommuters != m_weightCommuters) {
        m_weightCommuters = weightCommuters;
        emit this->weightCommutersChanged();
    }
}

int Controller::weightLocallyAttracted() const
{
    return m_weightLocallyAttracted;
}

void Controller::setWeightLocallyAttracted(int weightLocallyAttracted)
{
    if (weightLocallyAttracted != m_weightLocallyAttracted) {
        m_weightLocallyAttracted = weightLocallyAttracted;
        emit this->weightLocallyAttractedChanged();
    }
}

Controller::GlobalMotionType Controller::globalMotionType() const
{
    return m_globalMotionType;
}

void Controller::setGlobalMotionType(const GlobalMotionType &globalMotionType)
{
    if (globalMotionType != m_globalMotionType) {
        m_globalMotionType = globalMotionType;
        emit this->globalMotionTypeChanged();
    }
}

int Controller::tracingAppAccuracy() const
{
    return m_tracingAppAccuracy;
}

void Controller::setTracingAppAccuracy(int tracingAppAccuracy)
{
    if (tracingAppAccuracy != m_tracingAppAccuracy) {
        m_tracingAppAccuracy = tracingAppAccuracy;
        emit this->tracingAppAccuracyChanged();
    }
}

int Controller::percentOfTracingApps() const
{
    return m_percentOfTracingApps;
}

void Controller::setPercentOfTracingApps(int percentOfTracingApps)
{
    if (percentOfTracingApps != m_percentOfTracingApps) {
        m_percentOfTracingApps = percentOfTracingApps;
        emit this->percentOfTracingAppsChanged();
    }
}

int Controller::msBetweenSteps() const
{
    return m_msBetweenSteps;
}

void Controller::setMsBetweenSteps(int msBetweenSteps)
{
    if (msBetweenSteps != m_msBetweenSteps) {
        m_msBetweenSteps = msBetweenSteps;
        emit this->msBetweenStepsChanged();
    }
}

bool Controller::initialInfection() const
{
    return m_initialInfection;
}

void Controller::setInitialInfection(bool initialInfection)
{
    if (initialInfection != m_initialInfection) {
        m_initialInfection = initialInfection;
        emit this->initialInfectionChanged();
    }
}

Controller::AutomationType Controller::automationType() const
{
    return m_automationType;
}

void Controller::setAutomationType(const AutomationType &automationType)
{
    if (automationType != m_automationType) {
        m_automationType = automationType;
        emit this->automationTypeChanged();
    }
}

int Controller::populationCount() const
{
    return m_populationCount;
}

void Controller::setPopulationCount(int populationCount)
{
    if (m_renderPaintedItem && populationCount != m_populationCount) {
        m_populationCount = populationCount;
        emit this->populationCountChanged();
        this->restart();
    }
}

int Controller::infectionConstantSliderValue() const
{
    return m_infectionConstantSliderValue;
}

void Controller::setInfectionConstantSliderValue(int infectionConstantSliderValue)
{
    if (m_infectionConstantSliderValue != infectionConstantSliderValue) {
        m_infectionConstantSliderValue = infectionConstantSliderValue;
        for (auto p : m_population->persons()) {
            p->setInfectionConstant(sliderValueToInfectionConstant(infectionConstantSliderValue));
        }
        emit this->infectionConstantSliderValueChanged();
    }
}

void Controller::clicked(qreal x, qreal y)
{
    switch (m_interactionType) {
        case InteractionType::ISOLATE_20: this->interactionIsolate(x, y, 20); break;
        case InteractionType::ISOLATE_50: this->interactionIsolate(x, y, 50); break;
        case InteractionType::TRACK_20: this->interactionTrace(x, y, 20); break;
        case InteractionType::TRACK_50: this->interactionTrace(x, y, 50); break;
        case InteractionType::INFECT_1: this->interactionInfect(x,y,50,1); break;
        case InteractionType::INFECT_5: this->interactionInfect(x,y,50,5); break;
        case InteractionType::VACCINATE_20: this->interactionVaccinate(x, y, 20); break;
        case InteractionType::VACCINATE_50: this->interactionVaccinate(x, y, 50); break;


    }

//   MOUSE TRACING
//    m_renderPaintedItem->appendToMouseTrace(x,y);
//    m_renderPaintedItem->repaint();


}

void Controller::restart()
{
    m_lockdown = true;
    m_population->clear();
    m_diagramPaintedItem->clearValues();
    this->populatePopulation();
    m_lockdown = false;
    advance();
}

void Controller::lockdown()
{
    m_lockdown = !m_lockdown;
}

void Controller::halt()
{
    m_halted = !m_halted;
    if (!m_halted) advance();
}

void Controller::infectionsDownToZeroSlot()
{
    auto flashColor = QColor(Qt::green);
    if (m_renderPaintedItem->fillColor() != flashColor) {
        m_renderPaintedItem->setFillColor(flashColor);
        QTimer::singleShot(200, this, SLOT(infectionsDownToZeroSlot()));
    } else {
        m_renderPaintedItem->setFillColor(QColor(Qt::white));
    }
}

QPointF Controller::randomPosition() {
    return QPointF(
                m_dotRadius +fmod(fastrand(), m_renderPaintedItem->width() - 2*m_dotRadius),
                m_dotRadius + fmod(fastrand(),m_renderPaintedItem->height() -2 * m_dotRadius) );
}

QPointF Controller::randomBiasedPosition(QPointF attractor, qreal radius)
{
    qreal randRad = ( fmod(fastrand(),1000.0) * radius)/1000.0;

    qreal startX = std::min(attractor.x(), m_renderPaintedItem->width()-radius);
    startX = std::max(startX, radius);
    qreal startY = std::min(attractor.y(), m_renderPaintedItem->height()-radius);
    startY = std::max(startY, radius);

    qreal randDirection = Person::randomDirection();
    qreal deltaX = randRad*cos(randDirection);
    qreal deltaY = randRad*sin(randDirection);

    QPointF result;
    result.setX(startX-deltaX);
    result.setY(startY-deltaY);
    return result;
}

Person::MotionType Controller::randomPersonMotionType()
{
    int weightSum = m_weightLocallyAttracted + m_weightCommuters + m_weightTravellers + m_weightCouchPotatoes;
    int i = fastrand()%weightSum;

    if (i < m_weightLocallyAttracted) return Person::MotionType::LOCALLY_ATTRACTED;
    i -= m_weightLocallyAttracted;
    if (i < m_weightCommuters) return Person::MotionType::COMMUTER;
    i -= m_weightCommuters;
    if (i < m_weightTravellers) return Person::MotionType::TRAVELLER;
//    i -= m_weightTravellers;  // don't forget this when adding potential types
    return Person::MotionType::COUCH_POTATOE;

}

QVector<QPointF> Controller::adjacentGlobalAttractors(int count) {
    QVector<QPointF> result;
    if (count > 0) {
        // first
        result.append(m_globalAttractors.at(int(floor(fastrand()%m_globalAttractors.count()))));
        // scan distances
        QMap<qreal, QPointF> distanceMap;
        for (auto g : m_globalAttractors) {
            if (g!= result.first()) {
                distanceMap.insert(this->distanceSquared(result.first(), g), g);
            }
        }
        Q_ASSERT(count <= distanceMap.count()+1);  // +1 because result.first() is missing

        // shuffle the best contestance for a bit of variety
        auto candidates = distanceMap.keys();
        int swapLimit = std::min(count+2, candidates.count());
        for (int i = 0; i < 5; i++) {
            candidates.swapItemsAt(fastrand()%swapLimit, fastrand()%swapLimit);
        }
        for (int i = 0; i < count-1; i++) {
            result.append(distanceMap.value(candidates.at(i)));
        }
    }
    return result;
}

QVector<QPointF> Controller::anyGlobalAttractors(int count) {
    QVector<QPointF> result;
    for (int i = 0; i < count; i++) {
        result.append(m_globalAttractors.at(int(floor(fastrand()%m_globalAttractors.count()))));
    }
    return result;

}

void Controller::populatePopulation()
{

    if(!m_renderPaintedItem) return;
    int maxX = int(m_renderPaintedItem->width());
    int maxY = int(m_renderPaintedItem->height());

    // prepare attractors
    m_globalAttractors.clear();
    for (int i=0; i < m_numberOfAttractors; i++) {
        m_globalAttractors.append(QPointF(fastrand()%maxX, fastrand()%maxY));
    }

    QVector<Person*>* v = new QVector<Person*>();
    for (int i = 0; i < m_populationCount; i++) {

        auto pers = new Person();
        QQmlEngine::setObjectOwnership(pers, QQmlEngine::CppOwnership); // we instantiate it, we will destroy it

        if (m_globalMotionType == GlobalMotionType::MOVE_ATTRACTOR_BASED) {
            pers->setMotionType(this->randomPersonMotionType());
        } else {
            pers->setMotionType(Person::MotionType::RANDOM);
        }
        int radius = 0; // radius from home attractor in which this person may be initially located
        QVector<QPointF> attr; // all attractors this person will know / visit
        switch (pers->motionType()) {
            case Person::MotionType::COUCH_POTATOE: radius = 0; break;
            case Person::MotionType::TRAVELLER: attr = anyGlobalAttractors(4); radius = 400; break;
            case Person::MotionType::COMMUTER: attr = adjacentGlobalAttractors(2); radius = 100; break;
        case Person::MotionType::LOCALLY_ATTRACTED: attr = adjacentGlobalAttractors(1); radius = 100; break;
            case Person::MotionType::RANDOM: ; radius = -1; break;
        }
        pers->setAttractors(attr);
        if (attr.isEmpty()) {
            pers->setPos(this->randomPosition());
        } else {
            pers->setPos(this->randomBiasedPosition(attr.first(), radius));
        }

        pers->setHealthState(Person::HealthType::SUSCEPTIBLE);
        pers->setDirection(Person::randomDirection()); // possibly important, check right drift before removal
        pers->refineSpeedAndDirection();
        pers->setInfectionConstant(this->sliderValueToInfectionConstant(m_infectionConstantSliderValue));


        // pre-vaccination
        if (i / qreal(m_populationCount) < m_preVaccinated/100.0 ) {
            pers->vaccinate();
        }
        v->append(pers);
    }
    m_population->setPersons(*v);
    if (m_initialInfection) {
        m_population->persons().last()->infect();
    }
}

int Controller::rasterColFromX(qreal x, qreal width)
{
    Q_ASSERT(m_rasterCols == m_raster.first().count());
    auto colCount = m_rasterCols;
    auto cellWidth = width / colCount;
    return int(floor(x / cellWidth));
}

int Controller::rasterRowFromY(qreal y, qreal height)
{
    Q_ASSERT(m_rasterRows == m_raster.count());
    auto rowCount = m_rasterRows;
    auto cellHeight = height / rowCount;
    return int(floor(y / cellHeight));
}

// to speed up checking for a neighbour (normally quadratic cost) we first sort persons into areas on the screen.
// see neighbours(...
void Controller::insertIntoRaster(Person *p, qreal width, qreal height)
{
    int row = rasterRowFromY(p->pos().y(), height);
    int col = rasterColFromX(p->pos().x(), width);

    // after area size changes, a person might be caught off-screen
    // we obtain a currently valid position and recalculate the raster cell
    if (row < 0 || col < 0 || row >= m_rasterRows || col >= m_rasterCols) {
        p->setPos(this->randomPosition());
        row = rasterRowFromY(p->pos().y(), height);
        col = rasterColFromX(p->pos().x(), width);
    }
    m_raster[row][col].append(p);
}


// to speed up checking for a neighbour (normally quadratic cost) we first sort persons into areas on the screen.
// see insertIntoRaster(...
QVector<Person *> Controller::neighbours(Person *p, qreal radius, qreal width, qreal height)
{
    QVector<Person *> result;
    int pRow = rasterRowFromY(p->pos().y(), height);
    int pCol = rasterColFromX(p->pos().x(), width);
    // check for neighbours in this and in adjacent raster cells
    for (int row = pRow-1; row <= pRow+1; row++) {

        if (row < 0 || row >=m_rasterRows) continue;
        for (int col = pCol-1; col <= pCol+1; col++) {

            if (col < 0 || col >= m_rasterCols) continue;
            for (auto p2 : m_raster.at(row).at(col)) {
                if (p != p2 && distanceSquared(p, p2)<=radius*radius){
                    result.append(p2);
                }
            }
        }
    }
    return result;
}

//qreal Controller::distanceSquared(qreal x1, qreal y1, qreal x2, qreal y2)
//{
//    return (x1 - x2) * (x1 - x2)      // (delta x)^2
//            + (y1 - y2) * (y1 - y2);  // +(delta y)^2
//}


qreal Controller::distanceSquared(Person *p1, Person *p2)
{
    return (p1->pos().x() - p2->pos().x()) * (p1->pos().x() - p2->pos().x())      // (delta x)^2
            + (p1->pos().y() - p2->pos().y()) * (p1->pos().y() - p2->pos().y());  // +(delta y)^2
}
qreal Controller::distanceSquared(QPointF p1, QPointF p2)
{
    return (p1.x() - p2.x()) * (p1.x() - p2.x())      // (delta x)^2
            + (p1.y() - p2.y()) * (p1.y() - p2.y());  // +(delta y)^2
}

void Controller::sendSetToDiagram()
{
    int susceptible = 0;
    int vaccinated = 0;
    int infected = 0;
    int deceased = 0;
    int recovered = 0;
    int heavily = 0;
    for (auto p : m_population->persons()) {
        switch (p->healthState()) {
            case Person::HealthType::SUSCEPTIBLE: susceptible++; break;
            case Person::HealthType::VACCINATED: vaccinated++; break;
            case Person::HealthType::INFECTED_HEAVY:
            heavily ++;
            if (heavily > 20) {
                p->setWillDie(true);
            }
            [[fallthrough]];
            case Person::HealthType::INFECTED_LIGHT:
            case Person::HealthType::INFECTED_ASYMPT: infected++; break;
            case Person::HealthType::DECEASED: deceased++; break;
            case Person::HealthType::RECOVERED: recovered++; break;
        }
    }
    QVector<int> sums;
    sums << susceptible << vaccinated << recovered << deceased << infected;
    m_diagramPaintedItem->appendValueSet(sums);
}

// good enough for onClick, per person approach probably too slow (quadratic time), look at neigbours(...) solution
QVector<Person *> Controller::personsWithinDistanceFrom(qreal x, qreal y, qreal distance) {
    QVector<Person *> result;
    qreal left = x - distance;
    qreal right = x + distance;
    qreal top = y - distance;
    qreal bottom = y + distance;
    for (auto p : m_population->persons()) {
        //quick test
        if (p->pos().x() >= left && p->pos().x() <= right && p->pos().y() >= top && p->pos().y() <= bottom) {
            // thorough test
            if ((p->pos().x()-x)*(p->pos().x()-x)+(p->pos().y()-y)*(p->pos().y()-y) < distance * distance) {
                result.append(p);
            }
        }
    }
    return result;
}

Controller::InteractionType Controller::interactionType() const
{
    return m_interactionType;
}

void Controller::setInteractionType(const InteractionType &interactionType)
{
    if (m_interactionType != interactionType) {
        m_interactionType = interactionType;
        emit this->interactionTypeChanged();
    }
}

unsigned int Controller::s_seed=uint(QDateTime::currentDateTime().toMSecsSinceEpoch());
