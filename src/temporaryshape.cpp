/*
 * Copyright (C) 2020 Sebastian Diel
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>
 */

#include "temporaryshape.h"
#include "math.h"

TemporaryShape::TemporaryShape()
{

}

TemporaryShape::TemporaryShape(ShapeType shape, qreal x1, qreal y1, qreal x2, qreal y2)
{
    m_shape = shape;
    m_x1 = x1;
    m_y1 = y1;
    m_x2 = x2;
    m_y2 = y2;
}

qreal TemporaryShape::x1() const
{
    return m_x1;
}

void TemporaryShape::setX1(const qreal &x1)
{
    m_x1 = x1;
}

qreal TemporaryShape::y1() const
{
    return m_y1;
}

void TemporaryShape::setY1(const qreal &y1)
{
    m_y1 = y1;
}

qreal TemporaryShape::x2() const
{
    return m_x2;
}

void TemporaryShape::setX2(const qreal &x2)
{
    m_x2 = x2;
}

qreal TemporaryShape::y2() const
{
    return m_y2;
}

void TemporaryShape::setY2(const qreal &y2)
{
    m_y2 = y2;
}

int TemporaryShape::timeToLive() const
{
    return m_timeToLive;
}

void TemporaryShape::setTimeToLive(int timeToLive)
{
    m_timeToLive = timeToLive;
}

QColor TemporaryShape::color() const
{
    return m_color;
}

void TemporaryShape::setColor(const QColor &color)
{
    m_color = color;
}

qreal TemporaryShape::lineWidth() const
{
    return m_lineWidth;
}

void TemporaryShape::setLineWidth(const qreal &lineWidth)
{
    m_lineWidth = lineWidth;
}

TemporaryShape::ShapeType TemporaryShape::shape() const
{
    return m_shape;
}

void TemporaryShape::setShape(const ShapeType &shape)
{
    m_shape = shape;
}

bool TemporaryShape::advance() {
    m_timeToLive--;
    bool result = (m_timeToLive > 0);
    return result;
}

int TemporaryShape::test() const
{
    return m_test;
}

void TemporaryShape::setTest(int test)
{
    m_test = test;
}

void TemporaryShape::incTest()
{
    m_test++;
}

bool TemporaryShape::isCongruent(TemporaryShape *sh2)
{
    bool result = true;
    result &= fabs(m_x1 - sh2->x1()) < 0.01;
    result &= fabs(m_y1 - sh2->y1()) < 0.01;
    result &= fabs(m_x2 - sh2->x2()) < 0.01;
    result &= fabs(m_y2 - sh2->y2()) < 0.01;
    result &= m_shape == sh2->shape();
    return result;
}

