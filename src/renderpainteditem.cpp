/*
 * Copyright (C) 2020 Sebastian Diel
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>
 */

#include "renderpainteditem.h"

#include <QPainter>
#include <QElapsedTimer>

RenderPaintedItem::RenderPaintedItem()
{
    setAcceptedMouseButtons(Qt::AllButtons);
    setRenderTarget(QQuickPaintedItem::Image);
}

void RenderPaintedItem::paint(QPainter *painter)
{
    for (auto sh: m_tempShapes) sh->incTest();


    if (!m_population) return;
    QColor c;

    // draw temporary shapes
    QMutableVectorIterator<TemporaryShape*> shapeIter(m_tempShapes);
    while (shapeIter.hasNext()) {
        auto sh = shapeIter.next();
        painter->setPen(QPen(sh->color(),sh->lineWidth()));
        if (sh->shape() == TemporaryShape::ShapeType::LINE) {
            painter->drawLine(int(sh->x1()), int(sh->y1()), int(sh->x2()), int(sh->y2()));
        } else if (sh->shape() == TemporaryShape::ShapeType::ELLIPSE) {
            painter->setBrush(QBrush(QColor(Qt::transparent)));
            painter->drawEllipse(int(sh->x1()), int(sh->y1()), int(sh->x2()), int(sh->y2()));
        } else {
            Q_ASSERT(false) ;
        }
        if (!sh->advance()) {
            delete(sh);
            shapeIter.remove();
        }
    }

    // draw "person" circles
    for (auto p : m_population->persons()) {
        switch (p->visibleHealthState()) {      // delayed view
//        switch (p->healthState()) {             //immediate view
        case Person::HealthType::DECEASED: c = QColor(Qt::black); break;
        case Person::HealthType::RECOVERED: c = QColor(Qt::green); break;
        case Person::HealthType::SUSCEPTIBLE: c = QColor(Qt::blue); break;
        case Person::HealthType::INFECTED_HEAVY: c = QColor("#ff0000"); break;
        case Person::HealthType::INFECTED_LIGHT: c = QColor("#FF4444"); break;
        case Person::HealthType::INFECTED_ASYMPT: c = QColor(Qt::blue); break;
        case Person::HealthType::VACCINATED: c = QColor(Qt::darkGreen); break;
        }

        painter->setPen(QPen((p->inIsolation() ? QColor(Qt::cyan) :  c),(p->inIsolation() ? 4 :  0)));

        painter->setBrush(QBrush(c));
        if (p->healthState() == Person::HealthType::DECEASED) {
            painter->drawRect(int(p->pos().x()-m_dotRadius), int(p->pos().y()-m_dotRadius/6),int(m_dotRadius*2), int(m_dotRadius/3));
            painter->drawRect(int(p->pos().x()-m_dotRadius/6), int(p->pos().y()-m_dotRadius), int(m_dotRadius/3), int(m_dotRadius * 3));
        } else {
            painter->drawEllipse(int(p->pos().x()-m_dotRadius), int(p->pos().y()-m_dotRadius), int(2*m_dotRadius), int(2*m_dotRadius));
            if (p->visibleHealthState() == Person::HealthType::INFECTED_ASYMPT) {
                painter->setBrush(QBrush(QColor("#FF8888")));
                painter->drawEllipse(int(p->pos().x()-m_dotRadius/2), int(p->pos().y()-m_dotRadius/2), int(m_dotRadius), int(m_dotRadius));
            }
        }

    }

    // Mouse tracking
    //painter->setPen(QColor(Qt::green));
    //painter->setBrush(QBrush(QColor(Qt::green)));
    //for (auto pt : m_mouseTrace) {
    //    painter->drawPoint(pt.x(), pt.y());
    //    painter->drawEllipse(pt.x(), pt.y(),3,3);
    //}
}

void RenderPaintedItem::repaint()
{
    this->update(QRect(0,0,qRound(width()), qRound(height())));
}

void RenderPaintedItem::geometryChanged(const QRectF &newGeometry, const QRectF &oldGeometry)
{
    QQuickPaintedItem::geometryChanged(newGeometry, oldGeometry);
}

QVector<TemporaryShape *> RenderPaintedItem::tempShapes() const
{
    return m_tempShapes;
}

qreal RenderPaintedItem::dotRadius() const
{
    return m_dotRadius;
}

void RenderPaintedItem::setDotRadius(const qreal &dotRadius)
{
    m_dotRadius = dotRadius;
}

void RenderPaintedItem::appendToTemporaryShapes(TemporaryShape *shape)
{
    if (m_tempShapes.count() > 50) {
        shape->setTimeToLive(1);
    }
    // Replace any old line of same proportions
    QMutableVectorIterator<TemporaryShape*> iter (m_tempShapes);
    while (iter.hasNext()) {
        auto sh2 = iter.next();
        if (shape->isCongruent(sh2)) {
            delete sh2;
            iter.remove();
        } else {
            if  (m_tempShapes.count() > 50) {
                sh2->setTimeToLive(1);
            }
        }
    }
    m_tempShapes.append(shape);
}

Population *RenderPaintedItem::population() const
{
    return m_population;
}

void RenderPaintedItem::setPopulation(Population *population)
{
    m_population = population;
}
