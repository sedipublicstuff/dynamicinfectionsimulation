import QtQuick 2.0

Row {
    property alias text: titleName.text
    property int lineLength: 100
    anchors.bottomMargin: 2
    height: titleHeight
    Rectangle {
        y: textRec.y + textRec.height/2 - height/2
        width: lineLength
        height: 2
        color: "blue"
    }
    Rectangle {
        id: textRec
        height: titleName.contentHeight
        radius: 5
        y:  parent.height - height
        width: 300
        color: "blue"
        Text {
            id: titleName
            width: contentWidth + 5
            height: parent.height
            anchors.centerIn: parent

            color: "white"
            font.pointSize: 7
            verticalAlignment: Text.AlignBottom
            horizontalAlignment: Text.AlignHCenter
            font.bold: true
        }
    }
    Rectangle {
        y: textRec.y + textRec.height/2 - height/2
        width: lineLength
        height: 2
        color: "blue"
    }

}
